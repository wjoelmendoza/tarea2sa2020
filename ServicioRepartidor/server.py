#!/bin/python3
from flask import Flask
from flask_restful import Resource, Api
from random import randint

REPARTIDORES = {
            0: {'nombre': 'Carlos Estrada', 'vehiculo': 'unidad1'},
            1: {'nombre': 'Andrea Lopez', 'vehiculo': 'unidad9'},
            2: {'nombre': 'Andres Mencos', 'vehiculo': 'unidad11'},
            3: {'nombre': 'Monica Gallardo', 'vehiculo': 'unidad8'},
            4: {'nombre': 'Elver Suarez', 'vehiculo': 'unidad2'},
            5: {'nombre': 'Byron Elias', 'vehiculo': 'unidad7'},
            6: {'nombre': 'Erick Barrondo', 'vehiculo': 'unidad3'},
            7: {'nombre': 'Alejanda Burgos', 'vehiculo': 'unidad6'},
            8: {'nombre': 'Elisa Alvarado', 'vehiculo': 'unidad4'},
            9: {'nombre': 'Sergio Cortes', 'vehiculo': 'unidad5'},
        }


class Repartidor(Resource):
    """
    Esta clase hereda de Resource lo cual permite manejar a esta clase
    recursos del Api REST segun el verbo que se le indique
    """
    def __init__(self):
        """"
        En este constructor se definen los conductores
        que se encuentran registrados
        """
        Resource()
        '''
        coleccion de conductores y carros
        '''

    def get(self, direccion):
        """
        Se encarga de recuperar un conductor basado en el id que esta
        recibiendo
        :param id_piloto:
        :return: un diccionario con la informacion recolectada
        de la siguiente forma
        {
            'id': id_piloto,
            'nombre': nombre,
            'vehiculo': vehiculo,
            'destino': destino
        }
        """

        # recuperando info del repartidor
        print("obteniendo repartidor")
        input("presiona una tecla para continuar")
        id_piloto = randint(0, 9)
        conductor = REPARTIDORES.get(id_piloto, None)

        datos = {
            'id': id_piloto,
            'nombre': conductor['nombre'],
            'vehiculo': conductor['vehiculo'],
            'destino': direccion
        }

        return datos


class Servidor:
    """
    Esta clase se encarga de inicializar el servidor y sus recursos
    REST
    """
    def __init__(self):
        """
        Inicializa el servidor web y sus correspondientes recursos
        REST
        """
        self.app = Flask(__name__)
        self.api = Api(self.app)
        self.api.add_resource(Repartidor, '/repartidor/<direccion>')

    def iniciar(self):
        """
        Inicia el servidor web y sus recursos
        """

        '''
        host = 0.0.0.0. escuchar en todas las interfaces de red
        port = 8082, puerto en el que escucha
        debug = True, indica que el servidor esta en pruebas
        '''
        self.app.run(host='0.0.0.0', port=8082, debug=True)


if __name__ == '__main__':
    server = Servidor()
    server.iniciar()
