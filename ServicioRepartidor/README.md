# Servicio Repartidor
Este servicio se encarga de gestionar la información de los repartidores y lo expone en la dirección **http://localhost:8082/** mediante un api REST

## Recursos
### /repartidor/&laquo;direccion&raquo;
Este recurso utiliza el verbo http **GET**, recibe la &laquo;direccion&raquo; de destino y retorna la información del repartidor con el siguiente formato json:

    {
        id: id_piloto,
        nombre: nombre,
        vehiculo: vehiculo
        destino: direccion
    }

Donde:

* **id:** Es un valor entero que representa el identifica al piloto asignado.
* **nombre:** Es una cadena que representa el nombre del piloto asignado.
* **vehiculo:** Es una cadena que representa el modelo del vehículo asignado.
* **destino:** Es una cadena que representa la dirección donde debe ser entregada la orden.
